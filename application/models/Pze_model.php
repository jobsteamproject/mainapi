<?php
class Pze_model extends CI_Model
{
    public function getPze($id = null){
        if($id === null){
            $this->db->select("*");
            $this->db->from("tbl_amr");
            $this->db->limit(1);
            $this->db->order_by('id',"DESC");
            $query = $this->db->get();
            $result = $query->result();
            
            return $result;

        }else{
            return $this->db->get_where('tbl_amr',['id'  => $id]) ->result_array();
        }
    }
    public function createPze($data)
    {
        $this->db->insert('tbl_amr',$data);
        return $this->db->affected_rows();
    }

}
<?php
class Sensor_model extends CI_Model
{
    public function getSensor($id = null){
        if($id === null){
            $this->db->select("*");
            $this->db->from("tbl_iot");
            $this->db->limit(1);
            $this->db->order_by('id',"DESC");
            $query = $this->db->get();
            $result = $query->result();
            
            return $result;

        }else{
            return $this->db->get_where('tbl_iot',['id'  => $id]) ->result_array();
        }
    }
    public function createSensor($data)
    {
        $this->db->insert('tbl_iot',$data);
        return $this->db->affected_rows();
    }

}
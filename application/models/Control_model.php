<?php
class Control_model extends CI_Model
{
    public function getRelay($id = null){
        if($id === null){
 //           return $this->db->get('tbl_relay')->result_array();
            $this->db->select("*");
            $this->db->from("tbl_relay");
            $this->db->limit(1);
            $this->db->order_by('id',"DESC");
            $query = $this->db->get();
            $result = $query->result();
            
            return $result;

        }else{
            return $this->db->get_where('tbl_relay',['id'  => $id]) ->result_array();
        }
    }
    public function createControl($data)
    {
        $this->db->insert('tbl_relay',$data);
        return $this->db->affected_rows();
    }

}
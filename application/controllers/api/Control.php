<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Control extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        //memanggil nama model
        $this->load->model('Control_model', 'control');
    }

    //Minta data Request Method GET dalam bentuk JSON
    public function index_get()
    {
        //cek apakah ada ID ?
        $id = $this->get('id');
        if ($id === null){
            $control = $this->control->getRelay();
        }else{
            $control = $this->control->getRelay($id);
        }
        //var_dump($mahasiswa);

       if($control) {
        $this->response([
            'status' => true,
            'data' => $control
        ], REST_Controller::HTTP_OK); // Response OK
       }else{
        $this->response([
            'status' => false,
            'message' => 'id not found'
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
       }
    }


    //Menghapus data dengan Method DELETE

    // public function index_delete()
    // {
    //     $id = $this->delete('id');

    //     if ($id === null){
    //         $this->response([
    //             'status' => false,
    //             'message' => 'provide an id'
    //         ], REST_Controller::HTTP_BAD_REQUEST);
            
    //     }else{
    //         if($this->mahasiswa->deleteMahasiswa($id) > 0){
    //             //ok
    //             $this->response([
    //                 'status' => true,
    //                 'id' => $id,
    //                 'message' => 'deleted'
    //             ], REST_Controller::HTTP_OK); // Response OK
    //         }else{
    //             //id not found
    //             $this->response([
    //                 'status' => false,
    //                 'message' => 'id Not Found'
    //             ], REST_Controller::HTTP_BAD_REQUEST);
    //         }
    //     }
    // }


    //Menambah data baru dengan Method POST

    // public function index_post()
    // {

    //     //validasi dilakukan di client
    //     $data = [
    //         'nrp' => $this->post('nrp'),
    //         'nama' => $this->post('nama'),
    //         'email' => $this->post('email'),
    //         'jurusan' => $this->post('jurusan')
    //     ];

    //     if($this->mahasiswa->createMahasiswa($data) > 0){
    //         $this->response([
    //             'status' => true,
    //             'message' => 'new data has been created'
    //         ], REST_Controller::HTTP_CREATED); // Response Berhasil tambah data 
    //     }else{
    //         $this->response([
    //             'status' => true,
    //             'message' => 'failled to create new data'
    //         ], REST_Controller::HTTP_BAD_REQUEST); // Response Gagal tambah data 
    //     }
    // }

    //Mengubah data dengan Method PUT

    // public function index_put()
    // {
    //     $id = $this->put('id');
    //     $data = [
    //         'nrp' => $this->put('nrp'),
    //         'nama' => $this->put('nama'),
    //         'email' => $this->put('email'),
    //         'jurusan' => $this->put('jurusan')
    //     ];

    //     if($this->mahasiswa->updateMahasiswa($data, $id) > 0){
    //         $this->response([
    //             'status' => true,
    //             'message' => 'Data has been updated'
    //         ], REST_Controller::HTTP_OK); // Response Berhasil update data 
    //     }else{
    //         $this->response([
    //             'status' => true,
    //             'message' => 'failled to update data'
    //         ], REST_Controller::HTTP_BAD_REQUEST); // Response Gagal update data 
    //     }
    // }


    public function index_post()
    {

        //validasi dilakukan di client
        $data = [
            'relay1' => $this->post('relay1')
        ];

        if($this->control->createControl($data) > 0){
            $this->response([
                'status' => true,
                'message' => 'new data has been created'
            ], REST_Controller::HTTP_CREATED); // Response Berhasil tambah data 
        }else{
            $this->response([
                'status' => true,
                'message' => 'failled to create new data'
            ], REST_Controller::HTTP_BAD_REQUEST); // Response Gagal tambah data 
        }
    }
}
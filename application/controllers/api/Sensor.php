<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Sensor extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        //memanggil nama model
        $this->load->model('Sensor_model', 'sensor');
    }

    //Minta data Request Method GET dalam bentuk JSON
    public function index_get()
    {
        //cek apakah ada ID ?
        $id = $this->get('id');
        if ($id === null){
            $sensor = $this->sensor->getSensor();
        }else{
            $sensor = $this->sensor->getSensor($id);
        }
        //var_dump($mahasiswa);

       if($sensor) {
        $this->response([
            'status' => true,
            'data' => $sensor
        ], REST_Controller::HTTP_OK); // Response OK
       }else{
        $this->response([
            'status' => false,
            'message' => 'id not found'
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
       }
    }

    public function index_post()
    {

        //validasi dilakukan di client
        $data = [
            'temp' => $this->post('temp'),
            'humi' => $this->post('humi'),
            'pres' => $this->post('pres')
        ];

        if($this->sensor->createSensor($data) > 0){
            $this->response([
                'status' => true,
                'message' => 'new data has been created'
            ], REST_Controller::HTTP_CREATED); // Response Berhasil tambah data 
        }else{
            $this->response([
                'status' => true,
                'message' => 'failled to create new data'
            ], REST_Controller::HTTP_BAD_REQUEST); // Response Gagal tambah data 
        }
    }
}